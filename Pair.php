<?php

/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   tdd
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class Pair
{

    private $from;
    private $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function equals($object)
    {
        $pair = $object;
        return $this->from == $pair->from && $this->to == $pair->to;
    }

    public function hashCode() {
        return 0;
    }
}