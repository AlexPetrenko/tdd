<?php
/**
 * Created by PhpStorm.
 * User: apetrenko
 * Date: 26.05.15
 * Time: 11:09
 */

require_once('Money.php');
require_once('Expression.php');
require_once('Bank.php');
require_once('Sum.php');

class Calculator extends PHPUnit_Framework_TestCase {


	public function testMultiplication()
	{
		$dollarFive = new Money(5, 'USD');
		$money = $dollarFive->dollar(5);
		$this->assertEquals($money->dollar(10), $dollarFive->times(2));
		$this->assertEquals($money->dollar(15), $dollarFive->times(3));
	}

	public function testEquality()
	{
		$money = new Money(5, 'USD');
		$this->assertTrue( $money->dollar(5)->equals($money->dollar(5)));
		$this->assertFalse( $money->dollar(6)->equals($money->dollar(5)));
		$this->assertTrue( $money->frank(5)->equals($money->frank(5)));
		$this->assertFalse( $money->frank(6)->equals($money->frank(5)));

		$dollar = new Money(5, 'CHF');
		$this->assertFalse($money->frank(5)->equals($dollar->dollar(5)));
	}

	public function testCurrency()
	{
		$money = new Money(1, 'CHF');
		$dollar = $money->dollar(1);
		$frank = $money->frank(1);
		$this->assertEquals('USD', $dollar->currency());
		$this->assertEquals('CHF', $frank->currency());
	}

	public function testDifferentClassEquality()
	{
		$money = new Money(10, 'CHF');
		$this->assertTrue($money->equals(new Money(10, 'CHF')));
	}

	public function testPlusReturnsSum()
	{
		$money = new Money();
		$five = $money->dollar(5);
		$sum = new Sum($five, $five);

		$this->assertEquals($five, $sum->augend);
		$this->assertEquals($five, $sum->addend);
	}

	public function testReduceSum()
	{
		$money = new Money();
		$sum = new Sum($money->dollar(3), $money->dollar(4));
		$bank = new Bank();
		$result = $bank->reduce($sum, 'USD');

		$this->assertEquals($money->dollar(7), $result);
	}


	public function testReduceMoney()
	{
		$bank = new Bank();
		$money = new Money();
		$result = $bank->reduce($money->dollar(1), 'USD');
		$this->assertEquals($money->dollar(1), $result);
	}

	public function testReduceDifferentCurrency()
	{
		$bank = new Bank();
		$bank->addRate('CHF', 'USD', 2);
		$money = new Money();
		$result = $bank->reduce($money->frank(2), 'USD');
		$this->assertEquals($money->dollar(1), $result);
	}

	public function testIdentityRate()
	{
		$bank = new Bank();
		$this->assertEquals(1, $bank->rate('USD', 'USD'));
	}

	public function testMixedAddition()
	{
		$money = new Money();
		$fiveBucks = $money->dollar(5);
		$tenFrancs = $money->frank(10);
		$bank = new Bank();
		$bank->addRate('CHF', 'USD', 2);
		$result = $bank->reduce($fiveBucks->plus($tenFrancs), 'USD');
		$this->assertEquals($money->dollar(10), $result);
	}
}
