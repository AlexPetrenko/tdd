<?php

/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   tdd
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
class Sum implements Expression
{
    public $augend;
    public $addend;

    public function __construct(Money $augend, Money $addend)
    {
        $this->augend = $augend;
        $this->addend = $addend;
    }

    public function reduce(Bank $bank, $sum, $to)
    {
        $amount = $sum->augend->reduce($bank, $sum->augend, $to)->amount() +
            $sum->addend->reduce($bank, $sum->addend, $to)->amount();
        return new Money($amount, $to);
    }
}