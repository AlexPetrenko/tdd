<?php

/**
 * Description.
 * @copyright Copyright (c) Sigma Software
 * @package   tdd
 * @author    Alexey Petrenko <alex.petrenko@sigmaukraine.com>
 */
include_once 'Pair.php';


class Bank
{
    private $rates;

    public function reduce($source, $to)
    {
        return $source->reduce($this, $source, $to);
    }

    public function rate($from, $to)
    {
        if ($from == $to) {
            return 1;
        }
        $rate = $this->rates[$from . '-' . $to];
        return $rate['rate'];
    }

    public function addRate($from, $to, $rate)
    {
        $this->rates[$from . '-' . $to] = array(new Pair($from, $to),'rate' => $rate);
    }
}