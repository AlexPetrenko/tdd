<?php
/**
 * Created by PhpStorm.
 * User: apetrenko
 * Date: 29.06.15
 * Time: 18:15
 */
require_once('Expression.php');

abstract class abstractMoney {
	protected $amount;
	abstract public function equals($object);
	abstract public function times($multiplier);

}

class Money implements Expression
{

	protected $amount;
	protected $currency;

	public function __construct($amount = 0, $currency = '')
	{
		$this->amount = $amount;
		$this->currency = $currency;
	}

	public function equals($object)
	{
		return $this->amount === $object->amount && get_class($this) === get_class($object) && $this->currency === $object->currency;
	}

	public function times($multiplier)
	{
		return new Money($this->amount * $multiplier, $this->currency);
	}

	public function dollar($amount)
	{
		return new Money($amount, 'USD');
	}

	public function frank($amount)
	{
		return new Money($amount, 'CHF');
	}

	public function currency()
	{
		return $this->currency;
	}

	public function amount()
	{
		return $this->amount;
	}

	public function plus(Money $addend)
	{
		 return new Sum($this, $addend);
	}

	public function reduce(Bank $bank, $money, $to)
	{
		$rate = $bank->rate($money->currency(), $to);
		$amount = $money->amount() / $rate;
		return new Money($amount, $to);
	}
}
